package com.abb.task1.service.impl;

import com.abb.task1.service.SolutionService;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class SolutionServiceImpl implements SolutionService {


    @Override
    public int carParkingRoof(int[] cars, int k) {
        Arrays.sort(cars);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i + k - 1 < cars.length; i++) {
            min = Math.min(min, cars[i + k - 1] - cars[i] + 1);
        }
        return min;
    }
}