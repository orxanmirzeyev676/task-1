package com.abb.task1.service;

public interface SolutionService {

    int carParkingRoof(int[] cars, int k);
}
