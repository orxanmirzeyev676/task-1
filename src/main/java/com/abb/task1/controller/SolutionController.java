package com.abb.task1.controller;

import com.abb.task1.service.SolutionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/solution")
@RequiredArgsConstructor
public class SolutionController {

    private final SolutionService service;

    @PostMapping("/car-parking-roof")
    public int carParkingRoof(@RequestBody int[]cars, @RequestParam int k){
        return service.carParkingRoof(cars,k);
    }
}
